export class PortraitConfig extends FormApplication {
  constructor(object, options) {
    super(object || PortraitConfig.defaultSettings, options);
  }

  async getData() {
    let settings = await game.settings.get("sidebar-aesthetics", "portraitConfig");
    settings = mergeObject(PortraitConfig.defaultSettings, settings);
    return {
      data: settings,
      config: CONFIG.SidebarAesthetics,
    };
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      title: game.i18n.localize("SETTINGS.saRollPortraitN"),
      id: "portrait-config",
      template: "modules/sidebar-aesthetics/templates/settings/portrait.html",
      width: 480,
      height: "auto",
    });
  }

  static get defaultSettings() {
    return {
      size: "medium",
      borderColor: "#000000",
      showOn: {
        rawMessages: true,
        contextMessages: true,
      },
      portraitType: "portrait",
    };
  }

  activateListeners(html) {
    super.activateListeners(html);
    html.find('button[name="submit"]').click(this._onSubmit.bind(this));
  }

  /**
   * This method is called upon form submission after form data is validated.
   * @override
   */
  async _updateObject(event, formData) {
    const settings = expandObject(formData);
    await game.settings.set("sidebar-aesthetics", "portraitConfig", settings);
  }
}
